#include "esphome.h"

class Rad8Sensor : public Component, public UARTDevice {
 public:
  Sensor *spo2 = new Sensor();
  Sensor *bpm = new Sensor();
  Sensor *pi = new Sensor();

  // 01/09/00 09:57:29 SN=0000008467 SPO2=097% BPM=098 PI=01.70 ...

  // Lines output from the RAD8 are always a fixed length, which is 220.
  static const int max_line_length = 220;

  // The position of each metric in the line from the RAD8 is also fixed,
  // so we know from which position to start reading for each metric.
  // With above line as an example:
  const int start_spo2 = 37; // 097 starts at character 37
  const int start_bpm = 46; // 098 starts at character 46
  const int start_pi = 53; // 01.70 starts at character 53

  // Here we define how many characters to read for each data point:
  const int len_sp02 = 3; // 097 is 3 characters.  We don't want the %
  const int len_bpm = 3; // 098 is 3 characters
  const int len_pi = 5; // 01.70 is 5 characters

  // Variables to hold the data after we've read it from the terminal
  char b_spo2[4];
  char b_bpm[4];
  char b_pi[5];

  Rad8Sensor(UARTComponent *parent) : Component(), UARTDevice(parent) {}

  void setup() override {
  }

  int readline(int readch, char *buffer, int len)
  {
    static int pos = 0;
    int rpos;

    if (readch > 0) {
      switch (readch) {
        case '\n': // Ignore new-lines
          break;
        case '\r': // Return on CR
          rpos = pos;
          pos = 0;  // Reset position index ready for next time
          return rpos;
        default:
          if (pos < len-1) {
            buffer[pos++] = readch;
            buffer[pos] = 0;
          }
      }
    }
    // No end of line has been found, so return -1.
    return -1;
  }

  void i_sensor(Sensor* sensor, int value) {
    if (sensor->state != value) {
      if (value > 0) {
        sensor->publish_state(value);
      }
    }
  }

  void f_sensor(Sensor* sensor, float value) {
    if (sensor->state != value) {
      if (value > 0) {
        sensor->publish_state(value);
      }
    }
  }

  void loop() override {
    while (available()) {
      static char buffer[max_line_length];

      // If there is a line, read it into buffer
      if(readline(read(), buffer, max_line_length) > 0) {
        // ESP_LOGD("buffer", "%s", buffer);
        // Copy each data point in to the respective variables
        strncpy(b_spo2, buffer + start_spo2, len_sp02);
        strncpy(b_bpm, buffer + start_bpm, len_bpm);
        strncpy(b_pi, buffer + start_pi, len_pi);
        // ESP_LOGD("formatted", "spo2: %i, bpm: %i, pi: %f", atoi(b_spo2), atoi(b_bpm), atof(b_pi));
        // Convert and publish the values, e.g. turning 098 to 98, and 01.70 to 1.70
        i_sensor(spo2, atoi(b_spo2));
        i_sensor(bpm, atoi(b_bpm));
        f_sensor(pi, atof(b_pi));
      }
    }
  }
};