## Overview

This projects makes your Masimo Rad-87 pulse oximeter "smart", and is based off an article by [Mike Hinkle](https://www.influxdata.com/blog/monitoring-a-pulse-oximeter-with-influxdb/).

Mike's approach, for good reason, used an external raspberry pi (rpi), and as such he did not need to open his Rad-87. The rpi ran python code within the raspbian OS.

My approach uses an arduino-compatible ESP32, powered from the Rad-87's power supply, and is housed inside the chasis of the Rad-87.

The ESP32 is running ESPHome with a custom component, to read the data from the Rad-87's terminal, and output it to wherever ESPHome is configured to do so.

This allows me to store, analyze, and visualize my blood oxygen level (spo2), heartrate (bpm), and perfusion-index (pi).

Why do all of this - why not use one of the widely-available $15 pulse oximeters?

1. I do not trust their accuracy.
2. I care about my health and think my health is worth more than $15.
3. Few of them offer overnight data logging.
4. I did not want to rely on a phone app (e.g. Wellue brand bluetooth monitors)

Finally, I have included both the Rad-87 [Operator](./Masimo-Rad-87-Operators-Manual.pdf) and [Service](./Masimo-Rad-87-Service-Manual.pdf) manuals.  While you shouldn't need them, and they're not very difficult to find, having everything in one place is useful.

## Hardware

### Masimo Rad-87
Mine was bought broken on ebay, and I was lucky enough for it to be an easy fix.
They can be had for as little as $15 for a broken unit (if you want to try to fix it), or up to $200 or so for a working unit.

### ESP32
I used a `HiLetgo ESP-WROOM-32 ESP32 ESP-32S Development Board`.

### LM2596
A DC-DC adjustable step down voltage converter that is used to step the 12v power supply of the Rad-87 down to the 3.3v that the ESP32 requires.

The ESP32 that I used has a built-in voltage regulator that is capable of stepping 12v down to 3.3v, however using the LM2596 seemed more-better.

### MAX3232
A [MAX3232](https://www.sparkfun.com/products/11189) from sparkfun.
The serial pins of the Rad-87 operate at around 11v, which would fry the ESP32.
This device steps the 11v down to the 3.3v that the ESP32 requires.

### Wiring Diagram
```mermaid
flowchart TD
subgraph rad87
    subgraph powersupply
        12v
        gnd_ps[gnd]
    end

    subgraph serial_header
        pin5
        pin2
    end
end

subgraph lm2596
    in+
    in-
    out+
    out-
end

subgraph screw_terminal_1
    TX
    gnd_term1[gnd]
end

subgraph screw_terminal_2
    3.3v
    gnd_term2[gnd]
end

subgraph max3232
    R1IN
    V+
    GND
    R1OUT
end

subgraph esp32
    3.3V
    gnd_esp[gnd]
    GPIO36
end

12v --> in+
gnd_ps --> in-

pin2 --> TX
pin5 --> gnd_term1[gnd]

out+ --> 3.3v
out- --> gnd_term2[gnd]

TX --> R1IN
3.3v --> V+
gnd_term1 --- gnd_term2 --> GND

3.3v --> 3.3V
gnd_term2 --> gnd_esp[gnd]
R1OUT --> GPIO36
```

## Software

I am using ESPHome, with a custom component (`uart_rad8_sensor.h`), to read the data from the serial terminal of the Masimo Rad-87, and output it to influxdb.

The custom component defines 3 `sensors`: sp02, bpm, and pi.

In the main esphome config, a `text_sensor` is created with values from the 3 sensors; the `text_sensor` contains the line that is actually sent to influxdb.  It is done this way as we want all 3 sensors inside a single measurement, existing at a single point in time.

Finally, I use grafana to visual InfluxDB.

## Setup

I am running this on a macbook, and have installed esphome using pip, though the instructions should be the same for a linux machine.  If you're running windows, :shrug:

1. `git clone` this project, and `cd` into the cloned directory
2. `pip install requirements.txt`
3. Connect your ESP32 to your computer
4. Modify `main.yaml` lines 2-4 with your influxdb parameters.
5. Create `secrets.yaml` and add your wifi credentials:
```
ssid: my_ssid
password: my_ssid_password
```
6. `esphome run main.yaml`
